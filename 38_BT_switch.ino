#include <SoftwareSerial.h>
#include <Servo.h>

SoftwareSerial BTSerial(2, 3); //(TxD2, RxD3)
int motor =5;
int LED =12;
Servo  servo;

void setup() {
  Serial.begin(9600);
  BTSerial.begin(9600); //블루투스 통신 시작
  servo.attach(motor);
  pinMode (LED, OUTPUT);
}

void loop() {
  if(BTSerial.available())   
  {
    char bt;                
    bt = BTSerial.read();
      Serial.write(BTSerial.read());        
    if(bt == 'a')
      servo.write(0);               
      digitalWrite(LED, HIGH);
      delay(1000); 
    if(bt == 'b')
      servo.write(180);
      digitalWrite(LED, LOW);
      delay(1000);

  }
}
